using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    private Image background;
    private GameObject tutorialTxt;
    private bool isTutorialActive;
    private float lastSwitched;

    private Text content;

    private Logic logic;
    private TimeLog timeRecord;

    // Start is called before the first frame update
    void Start()
    {

        background = GetComponent<Image>();
        tutorialTxt = GameObject.Find("Canvas").gameObject;
        isTutorialActive = true;
        tutorialTxt.SetActive(isTutorialActive);

        content = tutorialTxt.GetComponent<Text>();

        logic = GameObject.Find("ExperimentLogic").GetComponent<Logic>();
        timeRecord = GameObject.Find("ExperimentLogic").GetComponent<TimeLog>();
        
    }

    // Update is called once per frame
    void Update()
    {

        //if (logic.currLvlNumb == 0)
        //{
        //    tutorialTxt.SetActive(true);
        //}
        //else
        //{
        //    tutorialTxt.SetActive(false);
            
        //}


        //if(OVRInput.GetUp((OVRInput.Button)CustomControls.TutorialSwitch)) //&& isCooldownOver)
        //{
        //    isTutorialActive = !isTutorialActive;
        //    background.enabled = isTutorialActive;
        //    tutorialTxt.SetActive(isTutorialActive);
        //    //lastSwitched = Time.time;
        //}
    }

}
