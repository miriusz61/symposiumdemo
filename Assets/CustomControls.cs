using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CustomControls
{
    Jump = OVRInput.Button.Two,
    //TutorialSwitch = OVRInput.Button.Three,
    StartGame = OVRInput.Button.One,
    //NextPreset = OVRInput.Button.Two,
    //ChoosePreset = OVRInput.Button.Four,
    //StartLog = OVRInput.Button.One,
    //SkipLvl = OVRInput.Button.PrimaryIndexTrigger,
    ForwardArrow = KeyCode.UpArrow,
    StartGameKeyboard = KeyCode.Space,
    //Old controls
    //Jump = OVRInput.Button.One,
    //LocoSwitch = OVRInput.Button.Two,
    //TutorialSwitch = OVRInput.Button.Three,
    //CheckPointReset = OVRInput.Button.Four
}
